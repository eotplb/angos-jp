# スワデシュ･リスト

スワデシュ･リストはモリス･スワデシュが1940年代から1950年代にかけて制作したものである。

## 引用元

	https://ja.wikipedia.org/wiki/スワデシュ・リスト
	https://en.wiktionary.org/wiki/Appendix:Swadesh_lists

- **翻訳の質を保証しません**。より適切な単語があるかも知れません。
- アンゴスの動詞の意味は**文脈に依存する**ため、単語を切り取って意味を断定出来るとは限りません。同じ単語が複数の設問で使われていても**文脈で全く意味が変わります**。
- この表では借入語を使っていません。**疑問符の付いているものは文脈に強く依存している**ため、補足説明がないと違う意味に解釈される可能性が高くなります。辞書で意味を確認する事を強く推奨します。
- 設問が複数の品詞を表す場合は語根のみ書いています。

## ライセンス

この文書全体の権利は以下の通りです。

**クリエイティブ･コモンズ 表示 - 継承 3.0 非移植 (CC BY-SA 3.0)**

	https://creativecommons.org/licenses/by-sa/3.0/deed.ja

このPDFのオリジナルソース(Markdown形式)は**GitLab**で公開しています。

	https://gitlab.com/eotplb/angos-jp

---

# アンゴス(Angos) スワデシュ･リスト 207

|⁠|英語|日本語|アンゴス|
|-:|-|-|-|
|1|I|私|wo|
|2|you|君 / あなた|to|
|3|he|彼|na‑lo|
|4|we|私達|le wo|
|5|you|君ら|le to|
|6|they|彼ら|le lo|
|7|this|これ|fo|
|8|that|あれ|do|
|9|here|ここ|fo|
|10|there|そこ|lo|
|11|who|誰|ki omo|
|12|what|何|ko|
|13|where|何処|ki oyo|
|14|when|何時|ki kaso|
|15|how|どのように|ku|
|16|not|否|nae|
|17|all|全て|os‑|
|18|many|多く|fal‑|
|19|some|少し|m‑|
|20|few|少ない|fet‑|
|21|other|他|ando|
|22|one|一|ayn|
|23|two|二|don|
|24|three|三|tin|
|25|four|四|kan|
|26|five|五|ken|
|27|big|大きい|kibi|
|28|long|長い|nineski|
|29|wide|広い|kwandui|
|30|thick|厚い|esteli ?|
|31|heavy|重い|olomi|
|32|small|小さい|ladi|
|33|short|短い|nemi|
|34|narrow|狭い|kwandu‑ladi|
|35|thin|細い / 薄い|fimbi ?|
|36|woman|女|nio / ni‑omo|
|37|man|男|nao / na‑omo|
|38|human|人|omo|
|39|child|子|anako|
|40|wife|妻|ni‑soyuso|
|41|husband|夫|na‑soyuso|
|42|mother|母|ni‑wano|
|43|father|父|na‑wano|
|44|animal|動物|hefo|
|45|fish|魚|ikano|
|46|bird|鳥|cilo|
|47|dog|犬|tesemo|
|48|louse|虱|hed‑ludeo|
|49|snake|蛇|ofido|
|50|worm|虫|ludeo|
|51|tree|木|fao|
|52|forest|森|fa‑efo|
|53|stick|枝|aelodo|
|54|fruit|実 / 果物|hwao|
|55|seed|種|mago|
|56|leaf|葉|ipo|
|57|root|根|layweno|
|58|bark|樹皮|fa‑eskino|
|59|flower|花|naso|
|60|grass|草|kusao|
|61|rope|縄|naluo|
|62|skin|皮|eskino|
|63|meat|肉|gosto|
|64|blood|血|sango|
|65|bone|骨|kosto|
|66|fat|脂|salo|
|67|egg|卵|ego|
|68|horn|角|cuno|
|69|tail|尻尾|eskolo|
|70|feather|羽|tolio|
|71|hair|髪|keyo|
|72|head|頭|hedo|
|73|ear|耳|elo|
|74|eye|目|wio|
|75|nose|鼻|buluno|
|76|mouth|口|belano|
|77|tooth|歯|tiso|
|78|tongue|舌|yaso|
|79|fingernail|爪|onikso|
|80|foot|足|gio|
|81|leg|脚|panao|
|82|knee|膝|wusio|
|83|hand|手|mano|
|84|wing|翼|panho|
|85|belly|腹|weyo|
|86|guts|腸|kikano|
|87|neck|首|galono|
|88|back|背|lugo|
|89|breast|胸|ni‑selao|
|90|heart|心|haleto|
|91|liver|肝|kabido|
|92|to drink|飲む|nestea|
|93|to eat|食べる|ala|
|94|to bite|噛む|tisa|
|95|to suck|吸う|esponya|
|96|to spit|吐く|cubaka|
|97|to vomit|吐く|hanyasa|
|98|to blow|吹く|bafela|
|99|to breathe|息する|feya|
|100|to laugh|笑う|usema|
|101|to see|見る|wia|
|102|to hear|聞く|ela|
|103|to know|知る|yada|
|104|to think|思う|noa|
|105|to smell|嗅ぐ|sauga|
|106|to fear|恐れる|fela|
|107|to sleep|寝る|sona|
|108|to live|生きる|eletea|
|109|to die|死ぬ|mawta|
|110|to kill|殺す|mawta|
|111|to fight|戦う|sukoba|
|112|to hunt|狩る|peta|
|113|to hit|打つ|iska|
|114|to cut|切る|bicaka|
|115|to split|割る|lisa|
|116|to stab|刺す|hanita|
|117|to scratch|掻く|lisa|
|118|to dig|掘る|hola|
|119|to swim|泳ぐ|ikana|
|120|to fly|飛ぶ|panha|
|121|to walk|歩く|gia|
|122|to come|来る|fea ?|
|123|to lie (as in a bed)|寝る / 横たわる|yalana|
|124|to sit|座る|sia|
|125|to stand|立つ|esa|
|126|to turn (自動詞)|回る|daila|
|127|to fall|落ちる|gelaa|
|128|to give|与える / 上げる|dala|
|129|to hold|持つ|ba|
|130|to squeeze|絞る|polia ?|
|131|to rub|擦る|lisa|
|132|to wash|洗う|sabona|
|133|to wipe|拭く|laholi|
|134|to pull|引く|foma ?|
|135|to push|押す|tekana ?|
|136|to throw|投げる|kelea|
|137|to tie|結ぶ|nalua|
|138|to sew|縫う|igne‑kamo|
|139|to count|数える|lakama|
|140|to say|言う|ansa|
|141|to sing|歌う|songa|
|142|to play|遊ぶ|gema|
|143|to float|浮く|dafea ?|
|144|to flow|流れる|yokea ?|
|145|to freeze|凍る|aysa|
|146|to swell|膨らむ|tena ?|
|147|sun|太陽|yango|
|148|moon|月|yino|
|149|star|星|yutuso|
|150|water|水|panio|
|151|rain|雨|yamulo|
|152|river|川|yokeo|
|153|lake|湖|hapyo|
|154|sea|海|lauto|
|155|salt|塩|sawto|
|156|stone|石|bato|
|157|sand|砂|noyo|
|158|dust|塵|hauco|
|159|earth|土|nehaso|
|160|cloud|雲|mego|
|161|fog|霧|mego|
|162|sky|空|yelo|
|163|wind|風|bafelo|
|164|snow|雪|lumio|
|165|ice|氷|ayso|
|166|smoke|煙|keo|
|167|fire|火|oto|
|168|ash|灰|efelo|
|169|to burn|燃える|ota|
|170|road|道|hodo|
|171|mountain|山|balo|
|172|red|赤|sang‑amo|
|173|green|緑|kusa‑amo|
|174|yellow|黄色|yang‑amo|
|175|white|白|lus‑amo|
|176|black|黒|osk‑amo|
|177|night|夜|osko|
|178|day|日|hio|
|179|year|年|taho|
|180|warm|暖かい|tepuli|
|181|cold|寒い|aysi|
|182|full|満ちた|osi ?|
|183|new|新しい|otali|
|184|old|古い|seni|
|185|good|良い|kali|
|186|bad|悪い|dimesi|
|187|rotten|腐った|fom‑tapya ? ede‑fomi ?|
|188|dirty|汚い|istin‑osk‑ami ? ede‑saboni ?|
|189|straight|真っ直ぐ|suoli|
|190|round|丸い|goli|
|191|sharp (as a knife)|鋭い|bicaki|
|192|dull (as a knife)|鈍い|ede‑bicaki ?|
|193|smooth|滑らか|kinui|
|194|wet|濡れた|panii|
|195|dry|乾いた|laholi|
|196|correct|正しい|pilafi|
|197|near|近い|nifei ?|
|198|far|遠い|wesei ?|
|199|right|右|suso|
|200|left|左|haso|
|201|at|で|de|
|202|in|で|ine|
|203|with|と|mwe|
|204|and|と|ye|
|205|if|もし|be|
|206|because|何故なら|isue|
|207|name|名前|namo|
