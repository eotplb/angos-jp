# Angos 日本語訳

アンゴス(Angos)の[新公式サイト](https://www.angos.org/)の日本語訳。不足している音節の説明を大幅に加筆しています。

## ライセンス

新公式サイトに表記はありませんが、旧公式サイトと同じ[クリエイティブ･コモンズ 表示-継承 3.0 非移植](https://creativecommons.org/licenses/by-sa/3.0/deed.ja)です。

翻訳の質を保証しません。修正には可能な限り応じますが、**義務を負いません**。

## PDF版

[Dropbox](https://www.dropbox.com/sh/thgrirh5p932zr5/AADyCntVFQf58V4x3Ue6aMBda?dl=0)からダウンロードして下さい(随時更新)。