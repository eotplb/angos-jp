# りんご文

りんご文とは、2014年9月20日にざすろん氏によって作成されたりんご文(ざすろんリスト)を皮切りに作られたいくつかの例文群のことである。これらの例文群は訳すことによってその人工言語に関する文法範疇の未決定である項目などを洗い出しするのを目的に作成された。

**人工言語学Wiki**より引用

	https://conlinguistics.wikia.org/ja/wiki/りんご文

【注意】 このリストの設問には日本語独自の表現が多く使われています。機械翻訳で他国の言葉に翻訳しても正確な翻訳にはなりません。

**Caution**: Many expressions unique to Japanese are used for the questions in this list. Even if you translate it into another country’s language by machine translation, it will not be an accurate translation.

- **翻訳の正確さを保証しません**。より最適かつ正確な表現があるかも知れません。
- 説明不足の設問を補完しています。
- 2通り以上に表記または解釈出来る設問は可能な範囲で何れの文も示しています。
- 借入語(非公式の単語)は斜体で表しています。

## オリジナルの引用元

**ざすろん**

	http://starlightensign.com/langcontents/1450 (リンク切れ)

**もやし**

	http://moyacis.tumblr.com/post/123468782425/57 (リンク切れ)

**Ziphil**

	http://ziphil.com/diary/conlang/115.html (前項のWikiには補足説明あり)

## ライセンス

この文書全体の権利は以下の通りです。

**クリエイティブ･コモンズ 表示 - 継承 3.0 非移植 (CC BY-SA 3.0)**

	https://creativecommons.org/licenses/by-sa/3.0/deed.ja

このPDFのオリジナルソース(Markdown形式)は**GitLab**で公開しています。

	https://gitlab.com/eotplb/angos-jp

---

## りんごを食べたい58文

2014年9月20日、**ざすろん氏**により作成されたりんご文。通称「ざすろんリスト」。

1. 私はりんごを食べる。
	- wo ala tofao.

2. 私はりんごを食べた。
	- wo me ala tofao.

3. 彼はりんごを食べている。
	- na‑lo koda ala tofao.

4. 彼女はりんごを食べ終わっている。
	- ni‑lo koneca ala tofao.

5. 彼女はりんごを食べ終わっていた。
	- ni‑lo me koneca ala tofao.

6. 私の妻はりんごを食べたことがある。
	- wi ni‑soyuso ba gogeo lae wi ni‑soyuso me ala tofao.
	- wi ni‑soyuso (lae wi ni‑soyuso) me ala tofao, ba gogeo.
	- wi ni‑soyuso lae me ala tofao, ba gogeo.

7. 私の妻はりんごを毎日食べる。
	- wi ni‑soyuso os‑hiu ala tofao.

8. 私と私の妻は昨日りんごを食べた。
	- wo ye wi ni‑soyuso me ala tofao de hiante hio.

9. 私と私の妻は6日前にりんごを食べた。
	- wo ye wi ni‑soyuso me ala tofao de sen hiante hio.

10. 彼らは明日りんごを食べる。(確定未来)
	- le na‑lo ala tofao de hiposte hio.

11. 彼らは6日後にりんごを食べる。(確定未来)
	- le na‑lo ala tofao de sen hiposte hio.

12. 彼女らは3日間りんごを食べている。(食事としてりんごだけを食べている)
	- le ni‑lo ala tofao hibye tin hio.

13. 彼女らはりんごを5分間食べ続けている。(りんごを食べる所要時間に5分かかっている)
	- le ni‑lo kodo ala tofao hibye ken minuto.

14. 彼は常にりんごを食べている。(四六時中ずっとりんごを食べている)
	- na‑lo os‑saminu ala tofao.

15. りんごが3つある。
	- tae tin tofao.
	- tin tofao taea.

16. りんご達がテーブルの上にある。
	- tae le tofao dafe masao.
	- le tofao taea dafe masao.

---

17. 誰かがあのりんごを食べてしまった。(その結果、あのりんごは今はない)
	- lo‑omo hiante me ala di tofao.

18. 誰かがこのりんごを食べそうだ。(推量)
	- lo‑omo ke ala fi tofao.

19. 誰かがこのりんご、そのりんご、あのりんごを食べたそうだ。(食べたと聞いた)(伝聞)
	- wo ela lae lo‑omo me ala fi tofao ye li tofao ye di tofao.

20. りんごがひとつもない。
	- nae tae tofao.
	- tofao nae taea.

21. 私はりんごを食べない。
	- wo nae ala tofao.

22. 私はりんごを食べられない。(りんごがないので状況的に食べられない)
	- wo nae bisaa ala tofao.

23. 私はりんごを床に落とした。
	- wo me gelaa tofao de lefelo.

24. りんごが床に落ちた。
	- tofao me gelaa de lefelo.

25. りんごは食べられない。(りんごの性質として、可食でなくなった)
	- tofao nae‑kod‑ali.

26. 私はりんごを食べたい。
	- wo desa tofao.

27. 私はりんごを買いたい。
	- wo desa maya de tofao.

28. このりんごは汚い。
	- fi tofao nae‑saboni.

29. このりんごは綺麗ではない。
	- fi tofao nae saboni.

30. あのりんごは食べられそうだ。
	- di tofao besela koda ala. (食べる事が可能に見える)
	- di tofao he hiante te ala de lo‑omo. (今まさに誰かに食べられる)

31. あれはりんごではない。
	- do nae tofao.

32. これはりんごではなくみかんだ。
	- fo nae tofao mice *mandalii* nalango.

33. あなたはみかんを食べるか?
	- ce to ala *mandalii* nalango? (単純な質問)
	- ce to *kutimu* ala *mandalii* nalango? (普段から)

34. はい、私はみかんを食べます。
	- se, wo ala *mandalii* nalango.

35. あなたはみかんを食べないのですか?
	- ce to nae ala *mandalii* nalango?

36. ええ、私はみかんを食べません。
	- nae, wo nae ala *mandalii* nalango.

37. あなたはみかんを食べたかった。ですよね?
	- to me desa *mandalii* nalango. ce nae?

38. いいえ、私はみかんを食べたくはありません。
	- nae, wo nae desa ala *mandalii* nalango.

39. あなたはりんごもみかんも食べないのですか? (*nor*)
	- ce to nae ala tofao oe *mandalii* nalango?

40. いや、私はりんごを食べます。
	- wo nae ala *mandalii* nalango mice wo ala tofao.

41. 私はみかんを食べない。だから、みかんは(私に)食べられない。(受け身の否定)
	- wo nae ala *mandalii* nalango. ne *mandalii* nalango nae te ala de wo.

42. りんごは食べることができない。なぜなら、りんごは綺麗ではないから。
	- tofao nae bisaa ala. isue tofao nae saboni.

43. りんごは汚い。一方で、みかんは綺麗だ。
	- tofao nae‑saboni. mice *mandalii* nalango saboni.

44. みかんを食べましょうよ?(勧誘)
	- ce sefame ala *mandalii* nalango?

45. いいえ、お断りします。
	- nae milo.

46. あそこでみかんを食べろ。りんごは食べるな。
	- ala *mandalii* nalango de di. nae ala tofao.

47. りんごを頂いても宜しいですか?みかんは差し上げます。
	- ce wo koda anya tofao? wo dala *mandalii* nalango de to.

48. 私は「りんごは食べるな、みかんを食べろ」と言った。
	- <nae ala tofao. ala *mandalii* nalango.> wo me ansa.

49. あなたはりんごを食べろと言うのですか?
	- ce to ansa de wo lae ala tofao?
	- ce to lae ala tofao, ansa de wo?

50. 貴様はりんごを食べたいと言った。
	- dimes‑to me ansa (lae dimes‑to) desa ala tofao.

51. 私はりんごを食べたいと言った覚えがない。
	- wo nae yada (lae wo) me ansa (lae wo) me desa ala tofao.

52. あなたは確か「私はりんごを食べたい」と言ったはずだ。
	- to istinu taluu me ansa lae \<wo desa ala tofao.>

53. 私はもしかしたらそう言ったかもしれない。
	- wo me nafasa ansa du.

54. あなたは絶対に私に対して「りんごを食べたい」と言った。
	- to istinu me ansa de wo lae \<wo desa ala tofao.>

55. 彼は私にりんごを食べさせる。
	- na‑lo te ala tofao de wo.

56. みかんは私に食べられていない。
	- *mandalii* nalango te ala de wo.

57. りんごは彼によって私に食べさせられた。
	- wo me te kaela fe na‑lo lae wo ala tofao fe na‑lo.
	- wo lae wo ala tofao fe na‑lo, me te kaela.
	- wo lae ala tofao fe na‑lo, me te kaela.

58. ここには何もない。
	- fo nei.

---

## リンゴを食べ足りない57文

リンゴを食べ足りない57文とは、2015年7月8日、**もやし氏**により作成されたりんご文。通称「もやしリスト」。

1. こんにちは。
	- anya.

2. 私はXXX(あなたの名前を入れて下さい)です。
	- wo XXX.
	- wi namo XXX.

3. これは何ですか?
	- fo ko?

4. これはリンゴです。
	- fo tofao.

5. これは大きなリンゴです。
	- fo kibi tofao.

6. これはあなたのリンゴですか?
	- ce fo ti tofao?

7. これは私のリンゴです。
	- fo wi tofao.

8. (私から近いところを指して)このリンゴは小さい。
	- fi tofao ladi.

9. (あなたから近いところを指して)そのリンゴは大きい。
	- li tofao kibi.

10. (私からもあなたからも遠いところを指して)あのリンゴはとても大きい。
	- di tofao istinu kibi.

11. リンゴは果実である。
	- tofao hwao.

12. とあるリンゴは大きい。
	- ayn‑tofao kibi.

13. 一般的にリンゴは甘い。
	- tofao kuwiu sugi.

14. 風が吹く。
	- bafelo bafela.

15. リンゴが落ちる。
	- tofao gelaa.

16. リンゴが私の前にある。
	- tofao taea ante wo.

17. 私はリンゴを掴む。
	- wo he ba tofao.

---

18. 私はあなたにリンゴを渡す。
	- wo dala tofao de to.

19. 私が渡すリンゴをあなたは食べる。
	- to ala tofao lae wo dala.
	- to lae wo dala, ala tofao.

20. (あなたは含まない)我々は(習慣として)リンゴを食べる。
	- le lo ala tofao ku kuwio.

21. あなたがたは(習慣として)リンゴを食べる。
	- le to ala tofao ku kuwio.

22. (あなたを含む)我々は(習慣として)リンゴを食べる。
	- le wo ala tofao ku kuwio.

23. 彼は(かつての習慣として)リンゴを食べていた。
	- na‑lo me ala tofao ku kuwio.

24. 彼女はリンゴを(習慣的に)食べるだろう。
	- ni‑lo ke ala tofao ku kuwio.

25. あなたはリンゴを食べ始める。
	- to he ala tofao.

26. あなたはリンゴを食べている。
	- to koda ala tofao.

27. あなたはリンゴを食べ終える。
	- to he koneca ala tofao.

28. 彼はリンゴを食べ始めるだろう。
	- na‑lo he ke ala tofao.

29. 彼はリンゴを食べ始めた。
	- na‑lo he me ala tofao.

30. 彼はリンゴを食べている。
	- na‑lo koda ala tofao.

31. 彼はリンゴを食べ終えるだろう。
	- na‑lo he koneca ke ala tofao.

32. 彼はリンゴを食べ終えていた。
	- na‑lo koneca me ala tofao.

33. このリンゴは小さかった。
	- fi tofao me ladi.

34. このリンゴは小さくない。
	- fi tofao nae ladi.

35. このリンゴはもっと大きくなるだろう。
	- fi tofao sele ke kiba.

36. 「彼はリンゴを食べていた。」とあなたは言う。
	- \<na‑lo me koda ala tofao.> to ansa.

37. 彼はリンゴを食べていたとあなたは言った。
	- to me ansa lae na‑lo me koda ala tofao.

38. 彼はリンゴを食べているとあなたは言った。
	- to me ansa lae na‑lo koda ala tofao.

39. 彼はリンゴを食べるだろうとあなたは言うだろう。
	- to ke ansa lae na‑lo ke ala tofao.

40. 彼はリンゴを食べるだろうとあなたは言った。
	- to me ansa lae na‑lo ke ala tofao.

41. 私はリンゴを食べられる可能性がある。
	- to ke koda bisaa ala tofao.

42. もしリンゴが食べられるならば、嬉しい。
	- be wo bisaa ala tofao, wo sayu balaka.

43. 私がリンゴを食べられないのは必然である。
	- lo talui lae wo nae bisaa ala tofao.

44. リンゴが食べられたら良いのになぁ。
	- wo watia lae wo bisaa ala tofao.
	- be wo bisaa ala tofao, wo sayu bahati.

45. リンゴを食べなければならない。
	- wo talua ala tofao.

46. リンゴを食べたほうが良い。
	- to sele sefame ala tofao.
	- be to bisaa ala tofao, to sayu sefame (ala tofao).

47. リンゴを食べるか食べないか選択できる。
	- to bisaa awkela lae to ala oe nae ala tofao.
	- to (lae to) ala oe nae ala tofao, bisaa awkela.

48. リンゴを食べない方が良い。
	- to nae sele sefame ala tofao
	- be to bisaa, to sayu nae sefame ala tofao.

49. リンゴを食べてはならない。
	- nae ala tofao.

50. 私は(能力的に)リンゴを食べられない。
	- wo nae koda tofao isue de mi bisao.

51. この大きくてかつ甘いリンゴを私は食べる。
	- wo ala tofao lae fi kibi ye sugi.

52. 私はこのリンゴまたはそのリンゴまたは、その両方を食べる。
	- wo ala fi tofao oe li tofao oe idu *anbaw*.

53. 彼は、大きいリンゴまたは甘いリンゴのどちらか一方のみを食べる。
	- na‑lo ala he kibi oe sugi tofao.
	- na‑lo he ala lae kibi oe sugi tofao.

---

54. あなたがそのリンゴ食べるならば、またその時に限り、私はこのリンゴを食べる。
	- be to ala li tofao ye he lo, wo sayu ala fi tofao.

55. 彼があのリンゴを食べるかどうかに関わらず、私はこのリンゴを食べる。
	- be to ala di tofao oe nae, wo sayu ala fi tofao.

56. 残念なことに、ここにリンゴはない。
	- nae bahati. de fo, nae tae tofao.

57. さようなら。
	- anya.

---

## りんごをより食べたい55文

りんごをより食べたい55文とは、2015年9月2日、**Ziphil氏**により作成されたりんご文。

1. このリンゴはあのリンゴより大きい
	- fi tofao sele kibi de di tofao.

2. あのリンゴより大きいリンゴ
	- tofao lae sele kibi de di tofao.

3. このリンゴはより大きい (比較対象の省略)
	- fi tofao sele kibi.

4. このリンゴはあのリンゴと同じくらい大きい
	- fi tofao balansi kibo de di tofao.

5. あのリンゴと同じくらい大きいリンゴ
	- tofao lae balansi kibo de di tofao.

6. このリンゴは同じくらい大きい (比較対象の省略)
	- fi tofao balansi kibo.

7. このリンゴはその箱の中で最も大きい
	- fi tofao safe kibi de li dohio.

8. その箱の中で最も大きいリンゴ
	- tofao lae safe kibi de li dohio.

9. このリンゴは最も大きい (比較範囲の省略)
	- fo tofao safe kibi.

10. 私はリンゴを彼より速く食べる (比較基準が副詞)
	- wo sele gatiu ala tofao de na‑lo.

11. 私はリンゴを彼と同じくらい速く食べる
	- wo balansu gatiu ala tofao de na‑lo.

12. 私はリンゴを家族の中で最も速く食べる
	- wo safe gatiu ala tofao de wi semyao.

13. このリンゴはあのリンゴより大きいか同じくらい大きい (このリンゴ≧あのリンゴ, 等号を含める)
	- fi tafao sele kibi oe basansi kibo de di tofao.

14. このリンゴはあのリンゴほど大きくない
	- fi tofao nae sele kibi de di tofao.

15. 私はあの人よりこの人が好きだ (私の好きの度合いについてあの人＜この人)
	- wo sele kala fi omo de di omo.

16. 私はあの人と同じくらいこの人が好きだ (私の好きの度合いについてあの人≒この人)
	- wo balansu kala fi omo de di omo.

17. 私はあの人がこの人を好きと思うよりもこの人が好きだ (あの人がこの人を好きな度合い＜私がこの人を好きな度合い, 上と言い分け可能か)
	- wo sele kala fi omo lae di omo noa lae kala fi omo.

---

18. 私はあの人がこの人を好きと思うのと同じくらいこの人が好きだ (あの人がこの人を好きな度合い≒私がこの人を好きな度合い)
	- wo balansu kala fi omo lae di omo noa lae kala fi omo.

19. このリンゴは以前より大きい (過去との比較)
	- fi tofao sele kibi de hiante.

20. このリンゴは以前と同じくらい大きい
	- fi tofao balansi kibo de hiante.

21. このリンゴは見かけより大きい
	- fi tofao sele kibi de li wio.

22. このリンゴは彼が想像していたのよりも大きい (比較対象が節の場合)
	- fi tofao sele kibi de na‑lo me koponaa.

23. このリンゴは彼が想像していたのと同じくらい大きい
	- fi tofao balansi kibo lae na‑lo me koponaa.
	- fi tofao lae na‑lo me koponaa, balansi kibo.

24. このリンゴは彼があのリンゴについて言っていたのよりも大きい (this apple is bigger than he said that that apple is)
	- fi tofao sele kibi de na‑lo me ansa kwe di tofao.

25. このリンゴは彼があのリンゴについて言っていたのと同じくらい大きい (this apple is as big as he said that that apple is)
	- fi tofao balansi kibo de na‑lo me ansa kwe di tofao.

26. 彼が昨日リンゴを食べていたのよりも速く私はこのリンゴを今食べることはできない
	- wo nae sele bisau he ala fi tofao de na‑lo hiante hio me ala tofao.

27. 花がいつか散ってしまうのと同じように私はこのリンゴを食べずにはいられない
	- naso ayn‑hiu ke gelaa, wo nae somu neu ala fi tifao.

28. できるだけ速くこのリンゴを食べなさい
	- lotane safe bisau gagiu ala fi tofao.

29. このリンゴはあのリンゴより少しだけ大きい
	- fi tofao sele fetu kibi de di tofao.

30. このリンゴはあのリンゴよりはるかに大きい
	- fi tofao sele istinu istinu kibi de di tofao.

31. このリンゴはあのリンゴより 7 cm 大きい
	- fi tofao sele sun *sencimetru* kibi de di tofao.

32. このリンゴはあのリンゴより幅が 7 cm 大きい
	- fi tofao sele sun *sencimetru* kuwandu‑kibi de di tofao.

33. このリンゴはあのリンゴの 3 倍大きい
	- fi tofao sele tin ce‑kibi kibo de di tofao.

34. このリンゴはあのリンゴより 20 % 大きい
	- fi tofao sele don‑den *posenti* kibi de di tofao.

---

35. このリンゴはその箱の中で 5 番目に大きい
	- fi tofao sol‑keni kibo de fi dohio.
	- fi tofao s5i kibo de fi dohio.

36. このリンゴはその箱の中で下から 5 番目に大きい (「5 番目に小さい」とは言わずに表現できるか)
	- fi tofao sol‑keni kibo fe subo de fi dohio.
	- fi tofao s5i kibo fe subo de fi dohio.

37. そのリンゴの価格は 400 円以上だ
	- wedo fe li tofao dadea oe nae akea kan‑syen *yenos*.

38. そのリンゴの価格は 400 円超だ (価格＞400 円)
	- wedo fe li tofao dafea kan‑syen *yenos*.

39. そのリンゴの価格は 400 円以下だ
	- wedo fe li tofao nae dafea kan‑syen *yenos*.

40. そのリンゴの価格は 400 円未満だ
	- wedo fe li tofao akea kan‑syen *yenos*.

41. そのリンゴは重さが少なくとも 2 kg はある
	- tafai wasnao fe fi naefeu don *kilogelamo*.

42. そのリンゴは重さが多くとも 2 kg しかない
	- tafai wasnao fe fi sefeu don *kilogelamo*.

43. リンゴは大きければ大きいほど甘い
	- ba tafao sele kibi, lo sayu sele sugi.

44. リンゴは大きければ大きいほど甘くない
	- ba tafao sele kibi, lo sayu nae sele sugi.

45. そのリンゴはまるで宝石のようだ
	- fi tafao kye *giwelo*.

46. 宝石のようなリンゴ
	- tofao lae lo kye *giwelo*.

47. 彼はアスリートのように速く走ることができる
	- na‑lo gatiu bisaa gati‑gia kwe kid‑gati‑gi‑gam‑omo.

48. アスリートのように速く走ることができる人
	- omo lae lo gatiu bisaa gati‑gia kwe kid‑gati‑gi‑gam‑omo.

49. 彼はまるでゴリラのようにそのリンゴを食べた
	- na‑lo me ala fi tofao kwe *golila*.

50. まるでゴリラのようにそのリンゴを食べる人
	- omo lae lo ala fi tofao kwe *golila*.

51. 彼はゴリラがバナナを食べるかのようにそのリンゴを食べた
	- na‑lo kwe me ala fi tofao lae *golila* ala banano.
	- na‑lo kwe lae *golila* ala banano, me ala fi tofao.

52. そのリンゴは飛び上がってしまうほど甘かった
	- fi tafao kwe me sugi lae lo dafea ilega.
	- fi tafao kwe lae lo dafea ilega, me sugi.

53. 飛び上がってしまうほど甘いリンゴ
	- kwe suga tofao lae lo dafea ilega.

54. あんなに甘いリンゴを私は食べたことがなかった
	- wo neu me ala lu sugi tofao.

55. ごちそうさまでした
	- wo mila to de istinu kali alo.
